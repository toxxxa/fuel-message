<?php return array(

    'template' => 'default',

    'delay_error'     => 0,         // Задержка перед скрытием сообщения, мсек
    'delay_warning'   => 5000,
    'delay_success'   => 5000,
    'delay_info'      => 0,

    'class_error'   => 'text-danger',
    'class_warning' => 'text-warning',
    'class_success' => 'text-success',
    'class_info'    => 'text-info',

);

