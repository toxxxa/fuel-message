<?php

namespace Message;

class Message
{
	/**
	 * default instance
	 *
	 * @var  array
	 */
	protected static $_instance = null;

	/**
	 * All the Asset instances
	 *
	 * @var  array
	 */
	protected static $_instances = array();

    public static function _init()
    {
        \Config::load('message', true);
        \Lang::load('message', true);
    }

	/**
	 * Return a specific instance, or the default instance (is created if necessary)
	 *
	 * @param   string  instance name
	 * @return  Instance
	 */
	public static function instance($instance = null)
	{
		if ($instance !== null)
		{
			if ( ! array_key_exists($instance, static::$_instances))
				return false;

			return static::$_instances[$instance];
		}

		if (static::$_instance === null)
			static::$_instance = static::forge();

		return static::$_instance;
	}

    /**
     * Gets a new instance of the Message class.
     *
     * @param string instance name
     * @param array $config
     * @return Instance
     */
	public static function forge($name = '_message', $config = array())
	{
		if ($exists = static::instance($name))
		{
			\Error::notice('Message instance with this name exists already, cannot be overwritten.');
			return $exists;
		}

		static::$_instances[$name] = new Instance($name, $config);

		if ($name == '_message')
			static::$_instance = static::$_instances[$name];

		return static::$_instances[$name];
	}

	/**
	 * You can not instantiate this class
	 */
	private function __construct() {}

	/**
	 * Adds an error message
	 *
	 * @param   string  $message  Message to add
	 * @return  $this
	 */
	public static function error($message)
	{
		return static::instance()->error($message);
	}

	/**
	 * Adds an info message
	 *
	 * @param   string  $message  Message to add
	 * @return  $this
	 */
	public static function info($message)
	{
		return static::instance()->info($message);
	}

	/**
	 * Adds a warning message
	 *
	 * @param   string  $message  Message to add
	 * @return  $this
	 */
	public static function warning($message)
	{
		return static::instance()->warning($message);
	}

	/**
	 * Adds a success message
	 *
	 * @param   string  $message  Message to add
	 * @return  $this
	 */
	public static function success($message)
	{
		return static::instance()->success($message);
	}

	/**
	 * Reset the error message store
	 *
	 * @return  $this
	 */
	public static function reset()
	{
		return static::instance()->reset();
	}

	/**
	 * Render error messages
	 *
	 * @return  string
	 */
	public static function render()
	{
		return static::instance()->render();
	}

	public function __toString()
	{
		return static::render();
	}

	/**
	 * Keep error message currently in the store
	 *
	 * @return  $this
	 */
	public static function keep()
	{
		return static::instance()->keep();
	}

	/**
	 * Returns if there are any messages in the queue or not
	 *
	 * @return  bool
	 */
	public static function any()
	{
		return static::instance()->any();
	}

	/**
	 * Get all messsages of a given type, or all if no type was given
	 *
	 * @return  array
	 */
	public static function get($type = null)
	{
		return static::instance()->get($type);
	}

	public static function add($type, $message)
	{
		$type = strtolower($type);
		if ( ! in_array($type, array('error','info','warning','success')))
			throw new \InvalidArgumentException("Unknown message type {$type}.");

		return static::instance()->{$type}($message);
	}

	/**
	 * Message aware alias for Response::redirect.
	 * Saves stored messages before redirecting.
	 */
	public static function redirect($url = '', $method = 'location', $code = 302)
	{
		return static::instance()->redirect($url, $method, $code);
	}

	public static function redirect_back($url = '', $method = 'location', $code = 302)
	{
		return static::instance()->redirect_back($url, $method, $code);
	}
}
