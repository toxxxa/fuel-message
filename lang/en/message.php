<?php return array(
    'text_error'    => 'Error',
    'text_warning'  => 'Warning',
    'text_success'  => 'Success',
    'text_info'     => 'Info',
);
