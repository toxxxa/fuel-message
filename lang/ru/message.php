<?php return array(
    'text_error'    => 'Ошибка',
    'text_warning'  => 'Предупреждение',
    'text_success'  => 'Сделано',
    'text_info'     => 'Информация',
);

