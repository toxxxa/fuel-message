<?php foreach (array('error', 'warning', 'success', 'info') as $type): ?>
<?php if($instance->get($type)): ?>
<div class="<?php echo \Config::get("message.class_{$type}"); ?>">
    <h4><?php echo __("message.text_{$type}"); ?></h4>
    <ul>
    <?php foreach ($instance->get($type) as $m): ?>
        <li><?php echo $m['body']; ?></li>
    <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>
<?php endforeach; ?>
<?php $instance->reset(); ?>
