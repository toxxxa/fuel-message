<?php if($instance->any()): ?>
    <div id="messages-module">
    <?php foreach (array('error', 'warning', 'success', 'info') as $type): ?>
        <?php if($instance->get($type)): ?>
        <div class="alert alert-dismissible alert-<?php echo $type === 'error' ? 'danger' : $type; ?>">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo __('message.text_'.$type); ?></h4>
            <ul class="list-unstyled">
            <?php foreach ($instance->get($type) as $m): ?>
                <li><?php echo $m['body']; ?></li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php $instance->reset(); ?>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            var wrapper = $('#messages-module');
            function alertClose() {
                $(this).alert('close');
            }
            <?php if($delay_error): ?>
            wrapper.find(".alert-error").delay(5000).slideUp(500, alertClose);
            <?php endif; ?>
            <?php if($delay_warning): ?>
            wrapper.find(".alert-warning").delay(5000).slideUp(500, alertClose);
            <?php endif; ?>
            <?php if($delay_success): ?>
            wrapper.find(".alert-success").delay(5000).slideUp(500, alertClose);
            <?php endif; ?>
            <?php if($delay_info): ?>
            wrapper.find(".alert-info").delay(5000).slideUp(500, alertClose);
            <?php endif; ?>
        });
    </script>
<?php endif; ?>
